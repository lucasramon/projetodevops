module.exports = {
    "collectCoverageFrom": ["src/**/*.js", "!**/node_modules/**"],
    "coverageReporters": ["html", "text", "text-summary", "cobertura"],
    "testMatch": ["**/*.test.js"],
    "testPathIgnorePatterns": [
        '/cypress',

    ],
    "transform": {
        "\\.[jt]sx?$": "babel-jest"
    },
}