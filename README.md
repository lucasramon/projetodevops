# Descrição do projeto

Este é um projeto simples de um jogo da velha feito em React, em que foi adicionado uma esteira de integração continua utilizando o GitLab, com os seguintes estágios:

* build
* testes unitários
* Checagem de estilo padrão utilizando lint
* testes e2e
* Criação de imagem Docker
* Push de imagem docker

As ferramentas que foram utilizadas para essas etapas foram:

* Jest para testes unitários
* esLint para checagem de estilo padrão
* cypress para testes e2e
