/// <reference types="cypress" />

describe('example to-do app', () => {
  beforeEach(() => {
    cy.visit('https://example.cypress.io/todo')
  })

  it('displays two todo items by default', () => {
    cy.get('.todo-list li').should('have.length', 2)
    cy.get('.todo-list li').first().should('have.text', 'Pay electric bill')
    cy.get('.todo-list li').last().should('have.text', 'Walk the dog')
  })

})

// Esta parte comentada contem o teste E2E que desenvolvi para o projeto, mas fiquei em duvida para fazer a chamada do metodo do cy.visit(), especificamente de qual URL utilizaria.
// Com isso, para que a pipeline rodasse normalmente, optei por manter o hello world que o cypress fornece, e deixei o meu codigo de testes abaixo.

// /// <reference types="cypress" />
// describe('Projeto Dev Ops', () => {
//   beforeEach(() => {

//     cy.visit('http://localhost:3000/')
//   })

//   it('Apresentar apenas um item das informações do Jogo por padrão', () => {

//     cy.get('.game-info ol').should('have.length', 1)


//     cy.get('.game-info ol').first().should('have.text', 'Go to game start')
//   })
// })